## Schéma

- Titre : Table de passage de Finess
<br />
- Clé(s) étrangère(s) : <br />
`ETA_NUM_EPMSI` => table [T_HADaa_nnE](/tables/T_HADaa_nnE) [ `ETA_NUM` ]<br />

### Liste des variables
<br />
<div>
    <a href="https://gitlab.com/healthdatahub/schema-snds/edit/master/schemas/PMSI/PMSI%20HAD/T_HADaa_nnGJ.json"  
    arget="_blank" rel="noopener noreferrer">> Éditer le schéma</a>
    <OutboundLink />
</div>
<br />

Nom|Type|Description|Propriétés
-|-|-|-
`ETA_NUM_GEO`|chaîne de caractères|N° FINESS de l’entité juridique||
`ETA_NUM_EPMSI`|chaîne de caractères|N° FINESS e-PMSI||
`ETA_NUM_JUR`|chaîne de caractères|N° FINESS de l’établissement (code géographique)||

