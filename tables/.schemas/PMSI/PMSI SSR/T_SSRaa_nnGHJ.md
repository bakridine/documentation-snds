## Schéma

- Titre : table des Groupes de Morbidité Dominante
<br />
- Clé(s) étrangère(s) : <br />
`ETA_NUM`, `RHA_NUM` => table [T_SSRaa_nnB](/tables/T_SSRaa_nnB) [ `ETA_NUM`, `RHA_NUM` ]<br />

### Liste des variables
<br />
<div>
    <a href="https://gitlab.com/healthdatahub/schema-snds/edit/master/schemas/PMSI/PMSI%20SSR/T_SSRaa_nnGHJ.json"  
    arget="_blank" rel="noopener noreferrer">> Éditer le schéma</a>
    <OutboundLink />
</div>
<br />

Nom|Type|Description|Propriétés
-|-|-|-
`ETA_NUM`|chaîne de caractères|N° FINESS de l&#x27;établisement||
`GHJ_COD`|chaîne de caractères|Code GMD||
`RHA_VER`|chaîne de caractères|N° version du format du RHA||
`NBR_JOU`|nombre réel|Nombre de jours||
`RHA_NUM`|chaîne de caractères|N° Séquentiel du séjour||

